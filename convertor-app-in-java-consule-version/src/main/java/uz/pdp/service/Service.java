package uz.pdp.service;

import uz.pdp.db.DB;
import uz.pdp.model.CurrenciesOfStates;

import static uz.pdp.tools.Scan.scanDouble;
import static uz.pdp.tools.Scan.scanString;


public class Service {


    public static void start() {
        System.out.println("1. Currency ==> Uzb:");
        System.out.println("2. Uzb ==> Currency:");
        System.out.println("3. Stop: ");
        System.out.print("Choose the command: ");
        String startCommand = scanString();
        switch (startCommand) {
            case "1": {
                currencyToUzb();
            }
            break;
            case "2": {
                uzbToCurrency();
            }
            break;
            case "3": {
                System.exit(0);
            }
            break;
            default:
                System.out.println("Error command!");
                start();
        }
    }

    public static void currencyToUzb() {
        double getUzbRate = 0;
        String info = "";
        System.out.println("\n1. Usd ==> UZS");
        System.out.println("2. Rub ==> UZS");
        System.out.println("3. Yuan ==> UZS");
        System.out.println("4. Back: ");
        System.out.print("Choose the command: ");
        String currencyCommand = scanString();
        switch (currencyCommand) {
            case "1": {
                for (CurrenciesOfStates currencies : DB.usdList.get(0)) {
                    getUzbRate = Double.parseDouble(currencies.getRate());
                    info = currencies.getCcyNmEN();
                    System.out.println("\n" + currencies.getNominal() + " (" + currencies.getCcyNmEN() + ") ==> " + currencies.getRate() + " so'm ");
                }
                System.out.print("Enter the amount ($): ");
                double sum = scanDouble();
                double sumUz = (sum * getUzbRate);
                System.out.println(sum + " (" + info + ") ==> " + sumUz + " so'm.");
                getUzbRate = 0;
                info = "";
                currencyToUzb();
            }
            break;
            case "2": {
                for (CurrenciesOfStates currencies : DB.ruList.get(0)) {
                    getUzbRate = Double.parseDouble(currencies.getRate());
                    info = currencies.getCcyNmEN();
                    System.out.println("\n" + currencies.getNominal() + " (" + currencies.getCcyNmEN() + ") ==> " + currencies.getRate() + " so'm ");
                }
                System.out.print("Enter the amount (rubl): ");
                double sum = scanDouble();
                double sumUz = (sum * getUzbRate);
                System.out.println(sum + " (" + info + ") ==> " + sumUz + " so'm.");
                getUzbRate = 0;
                info = "";
                currencyToUzb();
            }
            break;
            case "3": {
                for (CurrenciesOfStates currencies : DB.yuanList.get(0)) {
                    getUzbRate = Double.parseDouble(currencies.getRate());
                    info = currencies.getCcyNmEN();
                    System.out.println("\n" + currencies.getNominal() + " (" + currencies.getCcyNmEN() + ") ==> " + currencies.getRate() + " so'm ");
                }
                System.out.print("Enter the amount (yuan): ");
                double sum = scanDouble();
                double sumUz = (sum * getUzbRate);
                System.out.println(sum + " (" + info + ") ==> " + sumUz + " so'm.");
                getUzbRate = 0;
                info = "";
                currencyToUzb();
            }
            break;
            case "4": {
                start();
            }
            break;
            default:
                System.out.println("Error command!");
                currencyToUzb();
        }
    }


    public static void uzbToCurrency() {
        double getUzbRate = 0;
        System.out.println("\n1. UZS ==> Usd");
        System.out.println("2. UZS ==> Rub");
        System.out.println("3. UZS ==> Yuan");
        System.out.println("4. Back: ");
        System.out.print("Choose the command: ");
        String currencyCommand = scanString();
        switch (currencyCommand) {
            case "1": {
                for (CurrenciesOfStates currencies : DB.usdList.get(0)) {
                    getUzbRate = Double.parseDouble(currencies.getRate());
                }
                System.out.print("Enter the amount (so'm): ");
                double sum = scanDouble();
                double usd = (sum / getUzbRate);
                System.out.println(sum + " so'm ==> " + usd + "$ (usd)");
                getUzbRate = 0;
                uzbToCurrency();
            }
            break;
            case "2": {
                for (CurrenciesOfStates currencies : DB.ruList.get(0)) {
                    getUzbRate = Double.parseDouble(currencies.getRate());
                }
                System.out.print("Enter the amount (so'm): ");
                double sum = scanDouble();
                double rub = (sum / getUzbRate);
                System.out.println(sum + " so'm ==> " + rub + " (rubl)");
                getUzbRate = 0;
                uzbToCurrency();
            }
            break;
            case "3": {
                for (CurrenciesOfStates currencies : DB.yuanList.get(0)) {
                    getUzbRate = Double.parseDouble(currencies.getRate());
                }
                System.out.print("Enter the amount (so'm): ");
                double sum = scanDouble();
                double yuan = (sum / getUzbRate);
                System.out.println(sum + " so'm ==> " + yuan + " (rubl)");
                getUzbRate = 0;
                uzbToCurrency();
            }
            break;
            case "4": {
                start();
            }
            break;
            default:
                System.out.println("Error command!");
                currencyToUzb();
        }
    }


}
