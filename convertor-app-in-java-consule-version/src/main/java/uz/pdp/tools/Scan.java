package uz.pdp.tools;

import java.util.Scanner;

public class Scan {

    public static String scanString() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }

    public static double scanDouble() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextDouble();
    }


}
