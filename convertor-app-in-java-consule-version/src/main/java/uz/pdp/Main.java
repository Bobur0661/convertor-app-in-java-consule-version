package uz.pdp;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.SneakyThrows;
import uz.pdp.db.*;
import uz.pdp.model.*;
import uz.pdp.service.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class Main {

    @SneakyThrows
    public static void main(String[] args) {

//    Markaziy bank api ga bog'langan holda 3 valyuta (dollar, euro va yuan) bo'yicha valyutani so'mga yoki so'mni
//    valyutaga ayriboshlash imkonini beruvchi valyuta convertori dasturini tuzing.

        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        URL urlDollar = new URL("https://cbu.uz/oz/arkhiv-kursov-valyut/json/USD/");
        URL urlRub = new URL("https://cbu.uz/oz/arkhiv-kursov-valyut/json/RUB/");
        URL urlYuan = new URL("https://cbu.uz/oz/arkhiv-kursov-valyut/json/CNY/");

        URLConnection urlConnectionDollar = urlDollar.openConnection();
        URLConnection urlConnectionRub = urlRub.openConnection();
        URLConnection urlConnectionYuan = urlYuan.openConnection();

        BufferedReader readerDollar = new BufferedReader(new InputStreamReader(urlConnectionDollar.getInputStream()));
        BufferedReader readerRub = new BufferedReader(new InputStreamReader(urlConnectionRub.getInputStream()));
        BufferedReader readerYuan = new BufferedReader(new InputStreamReader(urlConnectionYuan.getInputStream()));

        CurrenciesOfStates[] currencyDollar = gson.fromJson(readerDollar, CurrenciesOfStates[].class);
        CurrenciesOfStates[] currencyRub = gson.fromJson(readerRub, CurrenciesOfStates[].class);
        CurrenciesOfStates[] currencyYuan = gson.fromJson(readerYuan, CurrenciesOfStates[].class);

        DB.usdList.add(currencyDollar);
        DB.ruList.add(currencyRub);
        DB.yuanList.add(currencyYuan);

        Service.start();

    }
}
